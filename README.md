> Braum 是一款web防护工具

braum可以很方便的帮助开发人员识别恶意请求

“有Braum在你们会很安全~”

![JDK](https://img.shields.io/badge/JDK-1.8-green.svg)
[![license](https://img.shields.io/badge/license-Apache2.0-yellow.svg)](https://gitee.com/yadong.zhang/braum-spring-boot-starter/blob/master/LICENSE)
[![QQ群](https://img.shields.io/badge/QQ群-435207435-orange.svg)](//shang.qq.com/wpa/qunwpa?idkey=5c07bfabe4ee47e5da1f10aa348311cc6f4fa4b4479d79d582648e02c979b4bd)

----
中文文档| [英文文档](https://gitee.com/yadong.zhang/braum-spring-boot-starter/blob/master/README_en.md)


# Braum业务处理流程

![Braum业务处理流程](https://images.gitee.com/uploads/images/2018/1101/144646_fcbd3874_784199.png "flow chart.png")

---

# 发展
2018-10-26 17:23 突然想开发一个Braum

2018-10-31 18:29 
- 调整代码结构、规范代码
- 优化代码：使用ScheduledExecutorService替换掉Timer，添加统一的Response处理结果类
- 增加启动Banner、统一异常类

2018-10-31 19:23
- 调整包结构
- 增加缓存分类属性
- 编写帮助文档

2018-11-04 20:36
- 增加redis支持

# 快速开始

#### 添加依赖

```xml
<dependency>
    <groupId>me.zhyd.braum.spring.boot</groupId>
    <artifactId>braum-spring-boot-starter</artifactId>
    <version>1.0.0-alpha</version>
</dependency>
```

#### 相关配置

```text
# 连续访问最高阀值，超过该值则认定为恶意操作的IP。单位：次 默认为20
braum.limit.access.threshold=20
# 间隔时间，在该时间内如果访问次数大于阀值，则记录为恶意IP，否则视为正常访问。单位：毫秒(ms)，默认为 5秒
braum.limit.access.interval=5000
# 当检测到恶意访问时，对恶意访问的ip进行限制的时间。单位：毫秒(ms)，默认为 1分钟
braum.limit.access.limitedTime=60000
# 黑名单存在的时间，在单位时间内用户访问受限的次数累加。单位：毫秒(ms)，默认为 1个月
braum.limit.access.blacklistTime=2592000000
# 缓存类型，默认为map存储,可选值（map、redis）
braum.limit.access.type=map
```

#### 开启Braum
在启动类上添加`@EnableBraumConfiguration`注解
```java
@SpringBootApplication
@EnableBraumConfiguration
public class BraumApplication {

    public static void main(String[] args) {
        SpringApplication.run(BraumApplication.class, args);
    }
}
```


#### 返回值

#### 正常
```text
{
  "code": 1,
  "msg": "[0:0:0:0:0:0:0:1]在5000毫秒内已连续发起 6 次请求",
  "expire": 0,
  "limitCount": 0,
  "accessInfo": {
    "ip": "0:0:0:0:0:0:0:1",
    "ua": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) Chrome/69.0.3497.100 Safari/537.36",
    "referer": "http://localhost:8080/",
    "requestUrl": "http://localhost:8080/",
    "params": ""
  }
}
```

#### 被限制
```text
{
  "code": 0,
  "msg": "[0:0:0:0:0:0:0:1]涉嫌恶意访问已被临时限制！共被限制过[3]次，本次剩余限制时间:59312 ms",
  "expire": 59312,
  "limitCount": 3,
  "accessInfo": {
    "ip": "0:0:0:0:0:0:0:1",
    "ua": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) Chrome/69.0.3497.100 Safari/537.36",
    "referer": "http://localhost:8080/",
    "requestUrl": "http://localhost:8080/",
    "params": ""
  }
}
```
| 字段  | 释义 |
| :------------: | :------------: |
| code | 响应码（1：正常，0：受限制） |
| msg | 返回内容 |
| expire | 当请求被限制时该值不为0，表示为被限制的剩余时间，单位毫秒 |
| limitCount | 当请求被限制时该值不为0，表示为被限制的次数 |
| accessInfo | 本次发起请求的内容 |

#### `accessInfo`字段

| 字段  | 释义 |
| :------------: | :------------: |
| ip | 当前访问IP |
| ua | 当前用户的UA |
| referer | 请求来源 |
| requestUrl | 当前请求的地址 |
| params | 当前请求的参数 |

#### 使用示例

修改配置文件
```text
# 阀值
braum.limit.access.threshold=2
# 间隔时间
braum.limit.access.interval=2000
# 单次限制的时间
braum.limit.access.limited-time=3000
```

###### 在controller中处理
```java
@RestController
public class BraumController {

    @Autowired
    BraumProcessor processor;
    @Autowired
    HttpServletRequest request;

    @RequestMapping("/")
    public Object index() {
        BraumResponse r = processor.process(request);
        if (r.getCode() == CommonConst.ERROR) {
            return "你已涉嫌恶意访问被临时禁止，请文明上网";
        }
        return "Hello world!";
    }
}
```

###### 在拦截器中使用
拦截器
```java
@Component
public class BraumIntercepter implements HandlerInterceptor {
    private static final Logger log = LoggerFactory.getLogger(BraumIntercepter.class);
    private static final int SUCCESS = 1;
    private static List<String> msgList = new ArrayList<>();

    static {
        msgList.add("Wow...您太冲动了，先喝杯咖啡冷静下。");
        msgList.add("Wow...一杯不够？那再来一杯。");
        msgList.add("还不够？再来一杯！");
        msgList.add("你就不怕被撑死么？");
        msgList.add("古恩吧，不接你这种客了");
        msgList.add("古恩!");
    }

    @Autowired
    private BraumProcessor processor;

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        BraumResponse r = processor.process(request);
        if (r.getCode() == SUCCESS) {
            return true;
        }
        String msg = r.getLimitCount() > msgList.size() ? msgList.get(msgList.size() - 1) : msgList.get(r.getLimitCount() - 1);
        log.info(msg);
        response.setCharacterEncoding("UTF-8");
        response.setContentType("text/html;charset=utf-8");
        PrintWriter writer = response.getWriter();
        writer.write(msg);
        writer.flush();
        writer.close();
        return false;
    }
}
```

访问测试
```text
2018-11-05 17:39:09.839 DEBUG 18020 --- [nio-8080-exec-3] m.z.b.spring.boot.BraumShieldProcessor   : [127.0.0.1]在2000毫秒内已连续发起 1 次请求
2018-11-05 17:39:10.633 DEBUG 18020 --- [nio-8080-exec-6] m.z.b.spring.boot.BraumShieldProcessor   : [127.0.0.1]在2000毫秒内已连续发起 2 次请求
2018-11-05 17:39:11.292 DEBUG 18020 --- [nio-8080-exec-8] m.z.b.spring.boot.BraumShieldProcessor   : [127.0.0.1]涉嫌恶意访问已被临时限制！共被限制过[1]次，本次剩余限制时间:3000 ms
2018-11-05 17:39:11.292  INFO 18020 --- [nio-8080-exec-8] com.example.demo.BraumIntercepter        : Wow...您太冲动了，先喝杯咖啡冷静下。
2018-11-05 17:39:19.573 DEBUG 18020 --- [io-8080-exec-10] m.z.b.spring.boot.BraumShieldProcessor   : [127.0.0.1]在2000毫秒内已连续发起 1 次请求
2018-11-05 17:39:20.155 DEBUG 18020 --- [nio-8080-exec-2] m.z.b.spring.boot.BraumShieldProcessor   : [127.0.0.1]在2000毫秒内已连续发起 2 次请求
2018-11-05 17:39:20.704 DEBUG 18020 --- [nio-8080-exec-4] m.z.b.spring.boot.BraumShieldProcessor   : [127.0.0.1]涉嫌恶意访问已被临时限制！共被限制过[2]次，本次剩余限制时间:3000 ms
2018-11-05 17:39:20.705  INFO 18020 --- [nio-8080-exec-4] com.example.demo.BraumIntercepter        : Wow...一杯不够？那再来一杯。
2018-11-05 17:39:28.242 DEBUG 18020 --- [nio-8080-exec-7] m.z.b.spring.boot.BraumShieldProcessor   : [127.0.0.1]在2000毫秒内已连续发起 1 次请求
2018-11-05 17:39:28.854 DEBUG 18020 --- [nio-8080-exec-9] m.z.b.spring.boot.BraumShieldProcessor   : [127.0.0.1]在2000毫秒内已连续发起 2 次请求
2018-11-05 17:39:29.332 DEBUG 18020 --- [nio-8080-exec-1] m.z.b.spring.boot.BraumShieldProcessor   : [127.0.0.1]涉嫌恶意访问已被临时限制！共被限制过[3]次，本次剩余限制时间:3000 ms
2018-11-05 17:39:29.332  INFO 18020 --- [nio-8080-exec-1] com.example.demo.BraumIntercepter        : 还不够？再来一杯！
2018-11-05 17:39:36.332 DEBUG 18020 --- [nio-8080-exec-3] m.z.b.spring.boot.BraumShieldProcessor   : [127.0.0.1]在2000毫秒内已连续发起 1 次请求
2018-11-05 17:39:36.855 DEBUG 18020 --- [nio-8080-exec-6] m.z.b.spring.boot.BraumShieldProcessor   : [127.0.0.1]在2000毫秒内已连续发起 2 次请求
2018-11-05 17:39:37.354 DEBUG 18020 --- [nio-8080-exec-7] m.z.b.spring.boot.BraumShieldProcessor   : [127.0.0.1]涉嫌恶意访问已被临时限制！共被限制过[4]次，本次剩余限制时间:3000 ms
2018-11-05 17:39:37.354  INFO 18020 --- [nio-8080-exec-7] com.example.demo.BraumIntercepter        : 你就不怕被撑死么？
2018-11-05 17:39:43.572 DEBUG 18020 --- [io-8080-exec-10] m.z.b.spring.boot.BraumShieldProcessor   : [127.0.0.1]在2000毫秒内已连续发起 1 次请求
2018-11-05 17:39:44.097 DEBUG 18020 --- [nio-8080-exec-2] m.z.b.spring.boot.BraumShieldProcessor   : [127.0.0.1]在2000毫秒内已连续发起 2 次请求
2018-11-05 17:39:44.589 DEBUG 18020 --- [nio-8080-exec-4] m.z.b.spring.boot.BraumShieldProcessor   : [127.0.0.1]涉嫌恶意访问已被临时限制！共被限制过[5]次，本次剩余限制时间:3000 ms
2018-11-05 17:39:44.590  INFO 18020 --- [nio-8080-exec-4] com.example.demo.BraumIntercepter        : 古恩吧，不接你这种客了
2018-11-05 17:39:49.342 DEBUG 18020 --- [nio-8080-exec-5] m.z.b.spring.boot.BraumShieldProcessor   : [127.0.0.1]在2000毫秒内已连续发起 1 次请求
2018-11-05 17:39:49.852 DEBUG 18020 --- [nio-8080-exec-8] m.z.b.spring.boot.BraumShieldProcessor   : [127.0.0.1]在2000毫秒内已连续发起 2 次请求
2018-11-05 17:39:50.293 DEBUG 18020 --- [io-8080-exec-10] m.z.b.spring.boot.BraumShieldProcessor   : [127.0.0.1]涉嫌恶意访问已被临时限制！共被限制过[6]次，本次剩余限制时间:3000 ms
2018-11-05 17:39:50.294  INFO 18020 --- [io-8080-exec-10] com.example.demo.BraumIntercepter        : 古恩!
2018-11-05 17:39:54.943 DEBUG 18020 --- [nio-8080-exec-3] m.z.b.spring.boot.BraumShieldProcessor   : [127.0.0.1]在2000毫秒内已连续发起 1 次请求
2018-11-05 17:39:55.389 DEBUG 18020 --- [nio-8080-exec-6] m.z.b.spring.boot.BraumShieldProcessor   : [127.0.0.1]在2000毫秒内已连续发起 2 次请求
2018-11-05 17:39:55.884 DEBUG 18020 --- [nio-8080-exec-7] m.z.b.spring.boot.BraumShieldProcessor   : [127.0.0.1]涉嫌恶意访问已被临时限制！共被限制过[7]次，本次剩余限制时间:3000 ms
2018-11-05 17:39:55.885  INFO 18020 --- [nio-8080-exec-7] com.example.demo.BraumIntercepter        : 古恩!
```



# 联系作者

 > yadong.zhang0415(a)gmail.com
 
<img src="https://gitee.com/yadong.zhang/static/raw/master/wx/wechat_account_1000x1000.jpg" width="300"/>

# 赞助

| 支付宝  | 微信  |
| :------------: | :------------: |
| <img src="https://gitee.com/yadong.zhang/static/raw/master/qrcode/zfb_code.png" width="200"/> | <img src="https://gitee.com/yadong.zhang/static/raw/master/qrcode/wx_code.png" width="200" /> |
